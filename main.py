import random
import tkinter as tk
from tkinter import messagebox,simpledialog
import time
#from memory_profiler import memory_usage
from tkinter import ttk
import psutil
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import tkinter.scrolledtext as st

currentgraph=0
choice_counts = {}
pire_cas={}
best_cas={}
def generate_random_preferences(num_students, num_institutions):
    # Générer aléatoirement des préférences pour les étudiants
    student_prefs = {
        f'etudiant{i}': random.sample([f'institution{j}' for j in range(num_institutions)], num_institutions)
        for i in range(num_students)
    }
    # Générer aléatoirement des préférences pour les institutions
    institution_prefs = {
        f'institution{i}': random.sample([f'etudiant{j}' for j in range(num_students)], num_students)
        for i in range(num_institutions)
    }

    return student_prefs, institution_prefs

def stable_marriage(student_preferences, school_preferences):
    # Créer des copies de listes des préférences pour l'algorithme
    student_prefs_copy = {s: prefs[:] for s, prefs in student_preferences.items()}
    students = list(student_prefs_copy.keys())
    schools = list(school_preferences.keys())

    # Initialiser le statut comme 'libres' pour tous les étudiants et écoles
    student_status = {student: None for student in students}
    school_status = {school: None for school in schools}
    student_free = students[:]

    # Tant qu'il reste des étudiants libres
    while student_free:
        student = student_free[0]
        school_pref_list = student_prefs_copy[student]
        school = school_pref_list.pop(0) # Prendre la première école préférée

        # Si l'école est libre, faire la paire
        if school_status[school] is None:
            school_status[school] = student
            student_status[student] = school
            student_free.remove(student)
        else:
            # Si l'école est prise, comparer les préférences
            current_partner = school_status[school]
            if school_preferences[school].index(student) < school_preferences[school].index(current_partner):
                # Si le nouvel étudiant est plus préféré, mettre à jour la paire
                school_status[school] = student
                student_status[student] = school
                student_free.remove(student)
                # L'ancien partenaire devient libre
                if current_partner not in student_free:
                    student_free.append(current_partner)

    return student_status

def calculate_satisfaction(pairing, preferences, reverse=False):
    # Calculer le score de satisfaction
    total_score = 0
    max_score = len(preferences)

    # Calculer le score pour chaque paire basé sur le rang de préférence
    for entity, partner in pairing.items():
        if reverse:
            rank = preferences[partner].index(entity) + 1
        else:
            rank = preferences[entity].index(partner) + 1
        score =((max_score - rank + 1 )/max_score)**rank # Un score plus haut pour une préférence plus haute
        
        total_score += score

    # Calculer la satisfaction moyenne et la normaliser
    normalized_satisfaction = total_score / len(pairing)

    return normalized_satisfaction

def plot_pie_chart(graph_frame):
    global currentgraph
    currentgraph=plot_pie_chart
    repetitions = int(repetition_entry.get())
    filtered_data = {}
    others = 0
    total = sum(choice_counts.values())
    for key, value in choice_counts.items():
        if value / total < 0.02:  # Less than 2%
            others += value
        else:
            filtered_data[key] = value/repetitions
    if others > 0:
        others = others/repetitions
        filtered_data['reste'] = others

    fig = Figure(figsize=(6, 4), dpi=100)
    ax = fig.add_subplot(111)
    ax.pie(list(filtered_data.values()), labels=[f' {k}' for k in filtered_data.keys()], autopct='%1.1f%%', startangle=140)
    ax.set_title("Voeu reçu par les Étudiants \n")
    ax.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    canvas = FigureCanvasTkAgg(fig, master=graph_frame)
    canvas_widget = canvas.get_tk_widget()
    canvas_widget.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
    canvas.draw()




def plot_bar_chart(graph_frame):
    global currentgraph
    currentgraph=plot_bar_chart
    repetitions = int(repetition_entry.get())
    #print(choice_counts)
    filtered_data = {k: v/repetitions for k, v in choice_counts.items() if v != 0}
    fig = Figure(figsize=(6, 4), dpi=100)
    ax = fig.add_subplot(111)
    ax.bar(range(len(filtered_data)), list(filtered_data.values()), align='center', alpha=0.7)
    ax.set_xticks(range(len(filtered_data)))
    ax.set_xticklabels([f' {i}' for i in filtered_data.keys()])
    ax.set_title("Voeu reçu par les Étudiants")
    ax.set_xlabel("Voeu Attribué")
    ax.set_ylabel("Nombre d'Étudiants")

    canvas = FigureCanvasTkAgg(fig, master=graph_frame)
    canvas_widget = canvas.get_tk_widget()
    canvas_widget.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
    canvas.draw()


def plot_line_graph(graph_frame):
    global currentgraph
    currentgraph=plot_line_graph
    repetitions = int(repetition_entry.get())
    filtered_data = {k: v/repetitions for k, v in choice_counts.items() if v != 0}
    fig = Figure(figsize=(6, 4), dpi=100)
    ax = fig.add_subplot(111)
    ax.plot(list(filtered_data.keys()), list(filtered_data.values()), marker='o')  # 'o' for markers on each point
    ax.set_xticks(list(filtered_data.keys()))
    ax.set_xticklabels([f' {i}' for i in filtered_data.keys()])
    ax.set_title("Voeu reçu par les Étudiants")
    ax.set_xlabel("Voeu Attribué")
    ax.set_ylabel("Nombre d'Étudiants")
    ax.grid(True)

    canvas = FigureCanvasTkAgg(fig, master=graph_frame)
    canvas_widget = canvas.get_tk_widget()
    canvas_widget.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
    canvas.draw()

def plot_extreme(graph_frame):
    global best_cas
    global pire_cas
def plot_scatter(data, title, xlabel, ylabel):
    # Filter out choices with 0 count
    filtered_data = {k: v for k, v in data.items() if v != 0}

    plt.figure()
    plt.scatter(list(filtered_data.keys()), list(filtered_data.values()))
    plt.xticks(list(filtered_data.keys()), [f' {i}' for i in filtered_data.keys()])
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid(True)
    plt.show()
    
def run_algorithm():
    global choice_counts
    try:
        num_students = int(student_entry.get())
        num_institutions = int(student_entry.get())
        repetitions = int(repetition_entry.get())

        # Vérifier si le nombre d'étudiants est égal au nombre d'institutions
        if num_students != num_institutions:
            output_text.delete('1.0', tk.END)
            output_text.insert(tk.END, "Erreur: Le nombre d'étudiants doit être égal au nombre d'institutions.\n")
            return

        process = psutil.Process()
        start_memory = process.memory_info().rss / (1024 * 1024)  # Convertir les bytes en MB
        start_time = time.time()

        # Générer les préférences et appliquer l'algorithme
        student_preferences, school_preferences = generate_random_preferences(num_students, num_institutions)
        result = stable_marriage(student_preferences, school_preferences)

        # Calculer le temps d'exécution et la mémoire utilisée
        end_time = time.time()
        end_memory = process.memory_info().rss / (1024 * 1024)
        execution_time = end_time - start_time
        memory_used = end_memory - start_memory

        # Calculer les statistiques de choix
        choice_counts = {i: 0 for i in range(1, num_students + 1)}
        for student, school in result.items():
            rank = student_preferences[student].index(school) + 1
            choice_counts[rank] = choice_counts.get(rank, 0) + 1

        # Calculer la satisfaction des étudiants et des institutions
        student_satisfaction = calculate_satisfaction(result, student_preferences)
        institution_satisfaction = calculate_satisfaction(result, school_preferences, reverse=True)
        satisfaction_data = {
        "Students": student_satisfaction,
        "Institutions": institution_satisfaction}

        # Afficher les résultats
        output_text.delete('1.0', tk.END)
        output_text.insert(tk.END, "Métriques de Performance:\n")
        output_text.insert(tk.END, f"Temps d'exécution: {execution_time:.2f} secondes\n")
        output_text.insert(tk.END, f"Mémoire utilisée: {memory_used:.2f} MB\n\n")
        #output_text.insert(tk.END, "Résultats des Appariements:\n")
        #for student, school in result.items():
        #    output_text.insert(tk.END, f"{student} est apparié avec {school}\n")

        output_text.insert(tk.END, "\nStatistiques des Choix:\n")
        for rank, count in choice_counts.items():
            if count != 0:
                output_text.insert(tk.END, f"Nombre d'étudiants avec choix {rank}: {count}\n")

        output_text.insert(tk.END, f"\nSatisfaction des étudiants: {student_satisfaction}\n")
        output_text.insert(tk.END, f"Satisfaction des institutions: {institution_satisfaction}\n")
        #plot_line_graph(choice_counts, "Distribution des Choix des Étudiants", "Choix", "Nombre d'Étudiants")
        #plot_bar_chart(choice_counts, "Distribution des Choix des Étudiants", "Choix", "Nombre d'Étudiants")
        #plot_pie_chart(choice_counts, "Distribution des Choix des Étudiants")
        #plot_scatter(choice_counts, "Diagramme de Dispersion des Choix des Étudiants", "Choix", "Nombre d'Étudiants")

    except ValueError:
        output_text.delete('1.0', tk.END)
        output_text.insert(tk.END, "Erreur: Veuillez saisir des nombres valides pour les étudiants et les institutions.\n")

def run_algorithm_multiple_times(repetitions):
    num_students = int(student_entry.get())
    num_institutions = int(student_entry.get())
    global choice_counts
    
    choice_counts = {i: 0 for i in range(1, num_students + 1)}  # Reset choice counts

    for _ in range(repetitions):
        student_preferences, school_preferences = generate_random_preferences(num_students, num_institutions)
        result = stable_marriage(student_preferences, school_preferences)

        for student, school in result.items():
            rank = student_preferences[student].index(school) + 1
            choice_counts[rank] = choice_counts.get(rank, 0) + 1
            


def prompt_for_repetitions():
    global currentgraph
    if currentgraph==0:
        currentgraph=plot_bar_chart
    global pire_cas
    global best_cas
    num_students = int(student_entry.get())
    num_institutions = int(student_entry.get())
    try:
        repetitions = int(repetition_entry.get())
        if repetitions > 0:
            output_text.delete('1.0', tk.END)  # Clear existing text
            #output_text.insert(tk.END, f"Métriques de Performance: {repetitions} fois...")
            total_student_satisfaction = 0
            total_institution_satisfaction = 0

            best_student_satisfaction = 0
            worst_student_satisfaction=float('inf')

            best_institution_satisfaction = 0
            worst_institution_satisfaction=float('inf')


            global choice_counts
            choice_counts = {i: 0 for i in range(1, num_students + 1)}  #
            total_memory_used = 0
            total_execution_time = 0

            for _ in range(repetitions):
                start_memory = psutil.Process().memory_info().rss / (1024 * 1024)  #MB
                start_time = time.time()

                student_preferences, school_preferences = generate_random_preferences(num_students, num_institutions)
                result = stable_marriage(student_preferences, school_preferences)
                
                student_satisfaction = calculate_satisfaction(result, student_preferences)
                institution_satisfaction = calculate_satisfaction(result, school_preferences, reverse=True)

                total_student_satisfaction += student_satisfaction
                total_institution_satisfaction += institution_satisfaction
                
                best_student_satisfaction = max(best_student_satisfaction, student_satisfaction)
                if best_student_satisfaction == student_satisfaction:
                    best_cas=choice_counts
                    
                best_institution_satisfaction = max(best_institution_satisfaction,institution_satisfaction)

                worst_student_satisfaction = min(worst_student_satisfaction, student_satisfaction)
                if worst_student_satisfaction == student_satisfaction:
                    pire_cas=choice_counts
                    
                worst_institution_satisfaction=min(worst_institution_satisfaction,institution_satisfaction)

                

                

                for student, school in result.items():
                    rank = student_preferences[student].index(school) + 1
                    choice_counts[rank] = choice_counts.get(rank, 0) + 1
                
                end_time = time.time()
                end_memory = psutil.Process().memory_info().rss / (1024 * 1024)  #MB
                execution_time = end_time - start_time
                memory_used = end_memory - start_memory

                total_execution_time += execution_time
                total_memory_used += memory_used
            average_student_satisfaction = total_student_satisfaction / repetitions
            average_institution_satisfaction = total_institution_satisfaction / repetitions
            #output_text.insert(tk.END, f"\nMoyenne de la satisfaction des étudiants: {average_student_satisfaction:.2f}\n")
            #output_text.insert(tk.END, f"Moyenne de la satisfaction des institutions: {average_institution_satisfaction:.2f}\n")

            #output_text.insert(tk.END, "\Multiple Execution:\n")
            
            output_text.insert(tk.END, f"\nTotal Execution Time: {total_execution_time:.2f} seconds \n")
            output_text.insert(tk.END, f"Total Memory Used: {total_memory_used:.2f} MB \n")
            output_text.tag_config('blue',foreground="blue")
            output_text.tag_config('green',foreground="green")
            output_text.tag_config('red',foreground="red")

            if(repetitions > 1):
                output_text.insert(tk.END, f"\nMoyenne de la satisfaction des étudiants: {average_student_satisfaction:.2f}\n",'blue')
                output_text.insert(tk.END, f"Moyenne de la satisfaction des institutions: {average_institution_satisfaction:.2f}\n",'blue')

                output_text.insert(tk.END, f"\nMeilleure satisfaction des étudiants: {best_student_satisfaction:.2f}\n",'green')
                output_text.insert(tk.END, f"Pire satisfaction des étudiants: {worst_student_satisfaction:.2f}\n",'red')
                
                output_text.insert(tk.END, f"\nMeilleure satisfaction des institution: {best_institution_satisfaction:.2f}\n",'green')
                output_text.insert(tk.END, f"Pire satisfaction des institution: {worst_institution_satisfaction:.2f}\n\n\n",'red')
            else :
                output_text.insert(tk.END, f"\nSatisfaction des étudiants: {average_student_satisfaction:.2f}\n",'blue')
                output_text.insert(tk.END, f"Satisfaction des institutions: {average_institution_satisfaction:.2f}\n\n\n",'blue')
            
            

            for rank, count in choice_counts.items():
                if count !=0:
                    output_text.insert(tk.END, f"Nombre d'étudiants avec choix {rank}: {count}\n")

            



            
            output_text.insert(tk.END,"\n")
        
        update_graph(currentgraph)
        app.geometry("1300x700")
    except ValueError:
        messagebox.showerror("Error", "Please enter a valid number.")




# interface
app = tk.Tk()
app.title("Stable Marriage")
app.geometry("420x150") 

style = ttk.Style()

style.configure('TButton', font=('Arial', 10), padding=10)
style.configure('TLabel', font=('Arial', 12), padding=10)
style.configure('TLabel2', font=('Arial', 12), padding=10)

style.configure('TEntry', font=('Arial', 12), padding=10)

frame = ttk.Frame(app, padding="10 10 10 10",width=400)
frame.pack(fill=tk.Y,side="left")
framer = ttk.Frame(app, padding="10 10 10 10")
framer.pack(fill=tk.BOTH, expand=True,side="right")
# Frame for buttons
button_frame = ttk.Frame(framer, padding="10 10 10 10")
button_frame.pack(side=tk.TOP, fill=tk.X)

# Frame for graph
graph_frame = ttk.Frame(framer)
graph_frame.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

# Function to update graph
def update_graph(plot_func):
    for widget in graph_frame.winfo_children():
        widget.destroy()
    plot_func(graph_frame)


# Buttons
bar_button = ttk.Button(button_frame, text="Bar Graph", command=lambda: update_graph(plot_bar_chart))
bar_button.pack(side=tk.LEFT, padx=10)


pie_button = ttk.Button(button_frame, text="Pie Chart", command=lambda: update_graph(plot_pie_chart))
pie_button.pack(side=tk.LEFT, padx=10)

line_button = ttk.Button(button_frame, text="Line Graph", command=lambda: update_graph(plot_line_graph))
line_button.pack(side=tk.LEFT, padx=10)

# Labels
ttk.Label(frame, text="Nombre de couples :").grid(column=0, row=0, sticky=tk.W)
ttk.Label(frame, text="Nombre de répétitions:").grid(column=0, row=1, sticky=tk.W)
repetition_entry = ttk.Entry(frame, width=15)
repetition_entry.insert(0, "1")  # Default value is 1
repetition_entry.grid(column=1, row=1)

student_entry = ttk.Entry(frame, width=15)
student_entry.grid(column=1, row=0)
"""
ttk.Label(frame, text="Nombre d'institutions:").grid(column=0, row=1, sticky=tk.W)
institution_entry = ttk.Entry(frame, width=15)
institution_entry.grid(column=1, row=1)

"""



button_width = 20 
run_button = ttk.Button(frame, text="Exécuter", command=prompt_for_repetitions)
run_button.grid(column=1, row=3)


"""
bar_button = ttk.Button(frame, text="Bar", command=lambda: plot_bar_chart(choice_counts, "Distribution des Choix des Étudiants", "Choix", "Nombre d'Étudiants"), width=button_width)
bar_button.grid(column=2, row=3,)

pie_button = ttk.Button(frame, text="Pie", command=lambda: plot_pie_chart(choice_counts, "Distribution des Choix des Étudiants"), width=button_width)
pie_button.grid(column=0, row=3,)

line_button = ttk.Button(frame, text="Line", command=lambda: plot_line_graph(choice_counts, "Distribution des Choix des Étudiants", "Choix", "Nombre d'Étudiants"), width=button_width)
line_button.grid(column=1, row=3,)
"""

# scrollbar
scrollbar = tk.Scrollbar(frame)
scrollbar.grid(column=2, row=5, sticky='ns')

output_text = tk.Text(frame, height=15, width=50, yscrollcommand=scrollbar.set)
output_text.grid(column=0, row=5, columnspan=2, sticky='nsew')
scrollbar.config(command=output_text.yview)

# Configure the grid to resize
frame.grid_rowconfigure(5, weight=1)
frame.grid_columnconfigure(1, weight=1)

def entrer(event):
    prompt_for_repetitions()

#shortcut
app.bind("<Return>",entrer)
app.mainloop()

