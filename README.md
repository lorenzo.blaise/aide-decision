# Aide a la décision

## Installation des Bibliothèques


Ce projet nécessite l'installation de certaines bibliothèques Python. Vous pouvez les installer automatiquement en utilisant le fichier requis.txt inclus dans ce projet.

pip install -r requis.txt

Cette commande installera automatiquement les bibliothèques psutil et matplotlib, qui sont nécessaires pour ce projet.

## Exécution du Programme

python main.py
